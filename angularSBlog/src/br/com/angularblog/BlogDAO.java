package br.com.angularblog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

@Startup
@ApplicationScoped
public class BlogDAO {
	
	@PostConstruct
	public void init(){
		System.out.println("BlogDAO.init()=====================================================================================================");
		
		
		for (int i = 0; i < 10; i++) {
			
			
			Blog b = new Blog(); 
			b.set_id((long)i);
			b.setBlogText("text do blog" + i);
			b.setIntroText("text do blog" + i);
			b.setDate(new Date());

			Comentario[] c = new Comentario[3];
			b.setComments(c);		
				for (int j = 0; j < 3; j++) {
					
					Comentario c1 = new Comentario();
					c1.set_id((long)j);
					c1.setCommentText("Comentario "+j);
					c[j] = c1;
					
				}
			
				blogList.add(b);
		}
		
		
		

		
		
	}
	
	private List<Blog> blogList = new ArrayList();
	
	
	public List<Blog> getBlogList(){
		return blogList;
	}
	
	
	

}
