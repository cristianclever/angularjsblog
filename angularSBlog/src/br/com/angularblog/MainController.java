package br.com.angularblog;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/blogservice")
public class MainController {

	@Inject
	BlogDAO dao;

	
	@GET
	@Path("/blogPostList")
	@Produces( { "application/json", "text/json" } )
	//http://localhost:8080/AngularJSBlog/services/blogservice/blogPost/2
	public List<Blog> findAll() {
		return  dao.getBlogList();
	}
	
	
	@GET
	@Path("/blogPost/{id}")
	@Produces( { "application/json", "text/json" } )
	//http://localhost:8080/AngularJSBlog/services/blogservice/blogPost/2
	public Blog findByID(@PathParam("id") Long id) {

		
		Blog b = null;
		
		
		
		List<Blog> blogList =  dao.getBlogList();
		
		for (int i = 0; i < blogList.size(); i++) {
			Blog bb = blogList.get(i);
			
			if(bb.get_id().longValue() == id.longValue()){
				b = bb;
				break;
			}
		}
		

		return b;

	}

}
