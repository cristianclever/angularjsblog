package br.com.angularblog;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Blog")
public class Blog {

	private Long _id;
	private Date date;
	private String introText;
	private String blogText;
	
	
	private Comentario[] comments;


	public Long get_id() {
		return _id;
	}


	public void set_id(Long _id) {
		this._id = _id;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getIntroText() {
		return introText;
	}


	public void setIntroText(String introText) {
		this.introText = introText;
	}


	public String getBlogText() {
		return blogText;
	}


	public void setBlogText(String blogText) {
		this.blogText = blogText;
	}


	public Comentario[] getComments() {
		return comments;
	}


	public void setComments(Comentario[] comments) {
		this.comments = comments;
	}

	
	
	
}
