/* chapter5/app.js */
'use strict';
/* App Module */
var blogApp = angular.module('blogApp', ['ngRoute','blogControllers']);

blogApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider.when('/', {
		templateUrl: 'resources/partials/main.html',
		controller: 'BlogCtrl'

	}).when('/blogPost/:id', {
		templateUrl: 'resources/partials/blogPost.html',
		controller: 'BlogViewCtrl'
	});
	
	$locationProvider.html5Mode(false).hashPrefix('!');
}]);


/*
organizacao:
blogApp[blogControllers[blogServices]]

*/