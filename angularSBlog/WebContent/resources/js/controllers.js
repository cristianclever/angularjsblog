/* chapter5/controllers.js 
 * Controllers 
 **/
'use strict';
var blogControllers = angular.module('blogControllers', ['blogServices','ngCookies']); //Injetou o outro modulo de 'blogservices', que foi um modulo criado separadamente


(function(angular) {
	//init
	alert('init')
	})(window.angular);





//Dentro do controller, precisa injetar o blogService como parametro (lembrar diego) 
//BLOG_POST é uma constante injetada

blogControllers.controller('BlogCtrl', ['$scope','$resource','$cookies','blogService','BLOG_POST','blogAjax','BlogHttpService','setCreds',  function BlogCtrl($scope,$resource,$cookies,blogService,BLOG_POST,blogAjax,BlogHttpService,setCreds) {
		$scope.blogArticle = "This is a blog post about AngularJS. We will cover how to build a blog and how to add comments to the blog post.";

		$scope.blogList= blogService.blogList;
		
		
		$scope.setCredentials = function(username,password){
			setCreds(username,password)
			
			//$cookies.put('username',username);
			//$cookies.put('password', password);
			

			var username_ = $cookies.get('username')
			alert(username_)
			
		}
		
		$scope.testFactory = function(){
			
			BlogHttpService.blog.get({id:1},
			
				function success(response) {
				console.log("Success:" + JSON.stringify(response));
			
				},
				function error(errorResponse) {
					console.log("Error:" + JSON.stringify(errorResponse));
				}			
			
			)

			
			
			
			//blogAjax.getBlogByID(1)
			//alert(BLOG_POST)

			//BlogHttpService.blogList.getAll()
			
			
		}

		
		
}]);


blogControllers.controller('BlogViewCtrl', ['$scope','$routeParams','blogService', function BlogViewCtrl($scope,$routeParams,blogService) {

	
	$scope.blogEntry = {};
	
	
	var blogId = $routeParams.id;
	
	var blogListSize = (blogService.blogList)? blogService.blogList.length:0;

	for(var i=0;i<blogListSize;i++){
		
		var currentBlog = blogService.blogList[i];
		if(currentBlog._id==blogId){
			$scope.blogEntry = currentBlog;
			break;
		}
	}
	

	
	
}]);