/*
Use Service when you need just a simple object such as a Hash, 
for example {foo:1, bar:2} 
It's easy to code, but you cannot instantiate it. 
A service is an injectable constructor. 
If you want you can specify the dependencies that you need in the function. 
A service is a singleton and will only be created once by AngularJS. 
Services are a great way for communicating between controllers like sharing data.
https://gist.github.com/demisx/9605099
*/

var blogServices = angular.module('blogServices', ['ngResource']);


// Constantes
blogServices.constant('BLOG_POST', 'http://localhost:8080/AngularJSBlog/services/blogservice/blogPost/:id');
blogServices.constant('BLOG_POST_LIST', 'http://localhost:8080/AngularJSBlog/services/blogservice/blogPostList');


blogServices.factory('BlogHttpService', ['$resource','BLOG_POST','BLOG_POST_LIST',	function($resource,BLOG_POST,BLOG_POST_LIST) {
	
	/*

https://docs.angularjs.org/api/ngResource/service/$resource

$resource(url, [paramDefaults], [actions], options);

	$resource('/user/:userId/card/:cardId',
	 {userId:123, cardId:'@id'}, {
		  charge: {method:'POST', params:{charge:true}}
		 });
	
	
	*/
	
	return{
		
		blog : $resource(BLOG_POST, {}, {
			get: {method: 'GET', cache: false, isArray: false},
			save: {method: 'POST', cache: false, isArray: false},
			update: {method: 'PUT', cache: false, isArray: false},
			delete: {method: 'DELETE', cache: false, isArray: false}
		}),
		blogList: $resource (BLOG_POST_LIST,	{}, {
			getAll: {method: 'GET', cache: false, isArray: true}
		})
		
	}
	
}]);



	
blogServices.factory('BlogPost', ['$resource','BLOG_POST',	function($resource,BLOG_POST) {
	
	
	//$resource(url, [paramDefaults], [actions], options);
	
	
	return $resource(BLOG_POST, {}, {
		get: {method: 'GET', cache: false, isArray: false},
		save: {method: 'POST', cache: false, isArray: false},
		update: {method: 'PUT', cache: false, isArray: false},
		delete: {method: 'DELETE', cache: false, isArray: false}
	});
}]);

blogServices.factory('BlogList', ['$resource','BLOG_POST',	function($resource,BLOG_POST_LIST) {
	return	$resource (BLOG_POST_LIST,	{}, {
		get: {method: 'GET', cache: false, isArray: true}
	});
}]);

blogServices.factory('blogAjax', 	function() {
	
	var getBlogByID = function (id){
		alert("ID:" + id);
		
	}
	
	return {
		getBlogByID: getBlogByID
	}; 
	
});




blogServices.service('blogService', function () {	
  this.blogList = [];	
});

//https://docs.angularjs.org/api/ngCookies/service/$cookies
blogServices.factory('setCreds',	['$cookies', function($cookies) {
			return function(un, pw) {
				
				$cookies.put('username',un);
				$cookies.put('password', pw);
				

			};
}]);







	